import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { createTutorials } from 'actions/tutorials';

export default function Add() {
  const initialTutorialState = {
    id: null,
    title: "",
    description: "",
    published: false
  }

  const [tutorial, setTutorial] = useState(initialTutorialState);
  const [submitted, setSubmitted] = useState(false);

  const dispatch = useDispatch();

  const handleInputChange = (e) => {
    setTutorial({...tutorial, [e.target.name]: e.target.value})
  }

  const saveData = async () => {
    try {
      const {title, description} = tutorial;

      const res = await dispatch(createTutorials(title, description));

      setTutorial({
        id: res.data.id,
        title: res.data.title,
        description: res.data.description,
        published: res.data.published
      });

      setSubmitted(true);

    } catch (error) {
      return Promise.reject(error)
    }
  }

  const newTutorial = () => {
    setTutorial(initialTutorialState);
    setSubmitted(false);
  };


  return (
     <div className="submit-form">
      {submitted ? (
        <div>
          <h4>You submitted successfully!</h4>
          <button className="btn btn-success" onClick={newTutorial}>
            Add
          </button>
        </div>
      ) : (
        <div>
          <div className="form-group">
            <label htmlFor="title">Title</label>
            <input
              type="text"
              className="form-control"
              id="title"
              required
              value={tutorial.title}
              onChange={handleInputChange}
              name="title"
            />
          </div>
          <div className="form-group">
            <label htmlFor="description">Description</label>
            <input
              type="text"
              className="form-control"
              id="description"
              required
              value={tutorial.description}
              onChange={handleInputChange}
              name="description"
            />
          </div>
          <button onClick={saveData} className="btn btn-success">
            Submit
          </button>
        </div>
      )}
    </div>
  )
}

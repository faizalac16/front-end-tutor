import React, {useState, useEffect} from 'react';
import { useDispatch } from 'react-redux';
import { findTutorialsByTitle, deleteData } from 'actions/tutorials';

export default function Tutorial() {

  const initialTutorialState = {
    id: null,
    title: "",
    description: "",
    published: false
  }

  const [tutorial, setTutorial] = useState([initialTutorialState]);
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();


  const fetchData = async () => {
    try {    
      let res = await dispatch(findTutorialsByTitle(title));
      setTutorial(res.data);
    } catch (error) {
      return Promise.reject(error)
    }
  }


  useEffect(() => {
    fetchData()
  },[title]);

  const handleDelete = async (id) => {
    try {
      await dispatch(deleteData(id));
      fetchData();
    } catch (error) {
      return Promise.reject(error)
    }
  }



  return (
    <div className="container">
      <input type="search" onChange={(e) => setTitle(e.target.value)} />
      {tutorial && tutorial.map((item,index) => 
        <div key={item.id} style={{flex:1, flexDirection: "column", justifyContent: "space-between", alignItems:"center"}}>
          <div>
            <h2>{item.title}</h2>
            <p>{item.description}</p>
          </div>
          <button onClick={() => handleDelete(item.id)}>Delete</button>
        </div>
      )}
    </div>
  )
}

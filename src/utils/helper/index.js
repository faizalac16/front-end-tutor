import {v4 as uuidv4} from "uuid";

export const renderTotal = (x,y) => {
    return x + y;
}

export const renderData = (data) => {
    let arr = [];

    for(const a of data){
        const obj = {
            key: uuidv4(),
            name: a.name,
            itemId: a.id
        }

        arr.push(obj)
    }

    return arr;
}
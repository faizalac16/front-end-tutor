import {
    RETRIEVE_TUTORIAL,
    REMOVE_ALL_TUTORIAL,
    REMOVE_TUTORIAL,
    CREATE_TUTORIAL,
    UPDATE_TUTORIAL
} from "actions/types";

const initialState = [];

const tutorialReducer = (tutorials = initialState, action) => {
    const {type, payload} = action;

    switch (type){
        case RETRIEVE_TUTORIAL:
            return payload;
        case CREATE_TUTORIAL:
            return [...tutorials, payload];
        case UPDATE_TUTORIAL:
            return tutorials.map((item) => {
                if(item.id === payload.id){
                    return {
                        ...item,
                        payload
                    }
                }else{
                    return item;
                }
            });
        case REMOVE_TUTORIAL:
            return tutorials.filter((item) => item.id !== payload.id);
        case REMOVE_ALL_TUTORIAL:
            return [];
        default:
            return tutorials;
    }
}

export default tutorialReducer;
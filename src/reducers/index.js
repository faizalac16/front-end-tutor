import { combineReducers } from "redux";
import tutorialReducer from "reducers/tutorials";

export default combineReducers({
    tutorialReducer
})
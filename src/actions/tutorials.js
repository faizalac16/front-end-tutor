import {
    RETRIEVE_TUTORIAL,
    REMOVE_ALL_TUTORIAL,
    REMOVE_TUTORIAL,
    CREATE_TUTORIAL,
    UPDATE_TUTORIAL
} from "actions/types";

import TutorialService from "api/service/TutorialService";


export const retrieveTutorials = () => async (dispatch) => {
    try {
        const res = await TutorialService.getAll();
        dispatch({
            type: RETRIEVE_TUTORIAL,
            payload: res.data
        });
        
        return Promise.resolve(res.data);
    } catch (error) {
        return Promise.reject(error);
    }
}

export const createTutorials = (title, description) => async (dispatch) => {
    try {
        const res = await TutorialService.postTutorials({title, description});
        dispatch({
            type: CREATE_TUTORIAL,
            payload: res.data.data
        });

        return Promise.resolve(res.data)
    } catch (error) {
        return Promise.reject(error)
    }
}

export const updateTutorials = (id, data) => async (dispatch) => {
    try {
        const res = await TutorialService.update(id, data);
        dispatch({
            type: UPDATE_TUTORIAL,
            payload: data
        });

        return Promise.resolve(res.data);
    } catch (error) {
        return Promise.reject(error)
    }
}

export const deleteData = (id) => async (dispatch) => {
    try {
        await TutorialService.remove(id);
        dispatch({
            type: REMOVE_TUTORIAL,
            payload: {id}
        });
    } catch (error) {
        return Promise.reject(error)
    }
}

export const deleteAllData = () => async (dispatch) => {
    try {
        const res = await TutorialService.removeAll();
        dispatch({
            type: REMOVE_ALL_TUTORIAL,
            payload: res.data
        })

        return Promise.resolve(res.data);
    } catch (error) {
        return Promise.reject(error)
    }
}

export const findTutorialsByTitle = (title) => async (dispatch) => {
    try {
        const res = await TutorialService.findByTitle(title);
        dispatch({
            type: RETRIEVE_TUTORIAL,
            payload: res.data.data
        });
        return Promise.resolve(res.data);
    } catch (error) {
        return Promise.reject(error)
    }
}
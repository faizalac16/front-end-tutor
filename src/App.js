import {BrowserRouter as Router, Link, Routes, Route} from "react-router-dom"
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from "react-bootstrap/Container";
import {Tutorial, Add} from "Pages";

function App() {

  return (
    <>
      <Router>
        <Navbar bg="dark" variant="dark">
          <Container>
            <Navbar.Brand href="#home">Navbar</Navbar.Brand>
            <Nav className="me-auto">
              <Link to={"/home"} className="nav-link">
                Home
              </Link>
              <Link to={"/tutorials"} className="nav-link">
                Tutorials
              </Link>
              <Link to={"/add"} className="nav-link">
                Add
              </Link>
            </Nav>
          </Container>
        </Navbar>
        <div className="mt-3">
          <Routes>
            <Route exact path="/tutorials" element={<Tutorial/>}/>
            <Route exact path="/" element={<Tutorial/>}/>
            <Route exact path="/add" element={<Add />} />
            <Route path="/tutorial/:id" element={<Tutorial />} />
          </Routes>
        </div>
      </Router>
    </>
  );
}

export default App;
